import React from "react";

interface INavigationBar {
  children: JSX.Element[] | JSX.Element
}

export default function NavigationBar({ children }: INavigationBar) {
  return <div><nav><ul>{children}</ul></nav></div>;
}
