import React from 'react';
import F from './F.svg';
import NavigationBar from './NavigationBar';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';

const App: React.FC = () => {
  return (
    <Router>
      <div>
        <NavigationBar >
          <li><Link to="/">Home!</Link></li>
          <li><Link to="/hey">Hey!</Link></li>
          <li><Link to="/plop">Plop!</Link></li>
        </NavigationBar>

        <Switch>
          <Route path="/hey">
            <div>HEY!</div>
          </Route>
          <Route path="/plop">
            <div>PLOP!</div>
          </Route>
          <Route path="/">
            <div>HELLO!</div>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
